1. Añadir al pom que el packaging va a ser de tipo jar: <packaging>jar</packaging>
2. Crear el jar con maven: mvn package
3. Esto nos creará dentro de la carpeta target un jar con el nombre en base al artifactId y la version que hayamos puesto en el pom
4. Para ejecutarlo debemos abrir una ventana de comandos y ejecutar el comando: java -jar ruta\fichero.jar