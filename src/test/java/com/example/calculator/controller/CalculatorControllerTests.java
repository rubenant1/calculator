package com.example.calculator.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.server.ResponseStatusException;

import io.restassured.module.mockmvc.RestAssuredMockMvc;

@SpringBootTest
class CalculatorControllerTests {

	@Autowired
	private WebApplicationContext webApplicationContext;
	
	@BeforeEach
	public void initialiseRestAssuredMockMvcWebApplicationContext() {
	    RestAssuredMockMvc.webAppContextSetup(webApplicationContext);
	}
	
	@Test
	void add_200() {		
		RestAssuredMockMvc.given()
			.when()
				.get("/api/add?param1=1.1&param2=2.2")
			.then()
				.log().ifValidationFails()
				.status(HttpStatus.OK)
				.body(equalTo("3.3"));
	}
	
	@Test
	void add_400() {
		RestAssuredMockMvc.given()
			.when()
				.get("/api/add?param1=1.1&param2=")
			.then()
				.log().ifValidationFails()
				.status(HttpStatus.BAD_REQUEST)
				.expect(result -> assertTrue(result.getResolvedException() instanceof ResponseStatusException));
	}
	
	@Test
	void subtract_200() {		
		RestAssuredMockMvc.given()
			.when()
				.get("/api/subtract?param1=2.2&param2=1.1")
			.then()
				.log().ifValidationFails()
				.status(HttpStatus.OK)
				.body(equalTo("1.1"));
	}
	
	@Test
	void subtract_400() {
		RestAssuredMockMvc.given()
			.when()
				.get("/api/subtract?param1=1.1&param2=")
			.then()
				.log().ifValidationFails()
				.status(HttpStatus.BAD_REQUEST)
				.expect(result -> assertTrue(result.getResolvedException() instanceof ResponseStatusException));
	}
}
