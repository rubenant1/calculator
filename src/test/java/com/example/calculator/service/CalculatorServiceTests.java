package com.example.calculator.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.calculator.service.impl.CalculatorServiceImpl;

class CalculatorServiceTests {

	CalculatorService service;
	
	@BeforeEach
	public void setUp() {
		service = new CalculatorServiceImpl();
	}
	
	@Test
	void testAddOK() {
		BigDecimal param1 = new BigDecimal(1);
		BigDecimal param2 = new BigDecimal(2);
		BigDecimal result = new BigDecimal(3);		
		
		assertEquals(0, result.compareTo(service.add(param1, param2)));
	}
	
	@Test
	void testSubtractOK() {
		BigDecimal param1 = new BigDecimal(2);
		BigDecimal param2 = new BigDecimal(1);
		BigDecimal result = new BigDecimal(1);		
		
		assertEquals(0, result.compareTo(service.subtract(param1, param2)));
	}
	
}
