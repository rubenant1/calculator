package com.example.calculator.service;

import java.math.BigDecimal;

public interface CalculatorService {
	
	BigDecimal add(BigDecimal param1, BigDecimal param2);
	BigDecimal subtract(BigDecimal param1, BigDecimal param2);

}
