package com.example.calculator.service.impl;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.example.calculator.service.CalculatorService;

import io.corp.calculator.TracerImpl;

@Service
public class CalculatorServiceImpl implements CalculatorService {
	
	private final TracerImpl tracer = new TracerImpl();	

	@Override
	public BigDecimal add(BigDecimal param1, BigDecimal param2) {
		BigDecimal result = param1.add(param2);
		
		tracer.trace(result);
		
		return result;
	}
	
	@Override
	public BigDecimal subtract(BigDecimal param1, BigDecimal param2) {
		BigDecimal result = param1.subtract(param2);
		
		tracer.trace(result);
		
		return result;
	}
}
