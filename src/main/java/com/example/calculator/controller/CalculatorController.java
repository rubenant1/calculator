package com.example.calculator.controller;

import java.math.BigDecimal;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.example.calculator.service.CalculatorService;

@RestController
public class CalculatorController {
	
	private CalculatorService service;
	
	public CalculatorController(CalculatorService service) {
		this.service = service;
	}
	
	@GetMapping(value="/api/add", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BigDecimal> add(BigDecimal param1, BigDecimal param2){
		if(param1 == null || param2 == null) {
			throw new ResponseStatusException(
			           HttpStatus.BAD_REQUEST, "Both parameters must be informed");
		}	
		
		return new ResponseEntity<>(service.add(param1, param2), HttpStatus.OK);
	}
	
	@GetMapping(value="/api/subtract", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BigDecimal> subtract(BigDecimal param1, BigDecimal param2){
		if(param1 == null || param2 == null) {
			throw new ResponseStatusException(
			           HttpStatus.BAD_REQUEST, "Both parameters must be informed");
		}
		
		return new ResponseEntity<>(service.subtract(param1, param2), HttpStatus.OK);
	}

}
